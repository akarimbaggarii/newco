import React from "react";
import "./Hero.css";
function Hero() {
  return (
    <div className="Hero">
      <div className="pb-6 sm:pb-8 lg:pb-12">
        <div className="max-w-screen-2xl px-4 md:px-8 mx-auto">
          <section className="flex flex-col lg:flex-row justify-between gap-6 items-center h-full sm:gap-10 section md:gap-16">
            <div className="xl:w-5/12 flex flex-col justify-center items-center lg:items-start sm:text-center lg:text-left lg:py-12 xl:py-24">
              <h1
                className="text-black-800 text-2xl sm:text-5xl md:text-6xl font-bold mb-8 md:mb-12"
                id="BoldText"
              >
                Discover how
                <span style={{ color: "#4C88FF" }}> Evold </span> makes <br />
                building products seamless
              </h1>

              <p className="lg:w-4/5 text-gray-500 paragraph xl:text-lg leading-relaxed mb-8 md:mb-12">
                Building products is our passion, We're pioneers in the use of
                research, analytical data, and technology to create engaging,
                valuable, and reliable products.
              </p>
              <div className="ButtonParent">
                <div className="buttons_grid">
                  <button
                    className="inline-block  mr-4 active:bg-indigo-700 focus-visible:ring  text-white text-sm md:text-base font-semibold text-center rounded outline-none transition duration-100 px-8 py-2"
                    id="bookCall"
                  >
                    Book a Discovery call
                  </button>
                  <button
                    className="inline-block presentation hidden bg-indigo-500 hover:bg-indigo-600 active:bg-indigo-700 focus-visible:ring ring-indigo-300  text-sm md:text-base font-semibold text-center rounded outline-none transition duration-100 px-14 py-2"
                    style={{ width: "220px" }}
                    id="CaseStudy"
                  >
                    Download Presentation
                  </button>
                  <button
                    className="inline-block bg-indigo-500 hover:bg-indigo-600 active:bg-indigo-700 focus-visible:ring ring-indigo-300  text-sm md:text-base font-semibold text-center rounded outline-none transition duration-100 px-8 py-2"
                    id="CaseStudy"
                  >
                    Case Study
                  </button>
                </div>
              </div>
              <div className="social_media flex mt-12">
                <h1>FOLLOW US</h1>
                <i class="fab fa-linkedin"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-facebook"></i>
                <i class="fab fa-instagram"></i>
              </div>
            </div>

            <div className="xl:w-5/12 h-48 lg:h-auto overflow-hidden flex justify-end">
              <div className="bg"></div>
              <div className="image h-full object-cover object-center"></div>
            </div>
          </section>
        </div>
      </div>
      ;
    </div>
  );
}

export default Hero;
